let problem4 = function(input_object) {
let combined_name = ""

let obj_length = Object.keys(input_object).length


for (let i = 0;i<obj_length;i++){
    let titleCaseTerm = ""
    let inputTerm = Object.values(input_object)[i]
    let lowerInputTerm = inputTerm.toLowerCase()
    let firstLetterUpper = lowerInputTerm[0].toUpperCase()
    let secondTermLower = lowerInputTerm.slice(1,)
    let convertedInputTerm = firstLetterUpper+secondTermLower
    combined_name = combined_name + " " + convertedInputTerm
}
return combined_name

}

module.exports = problem4
