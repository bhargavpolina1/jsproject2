let problem3 = function(input_date) {
let  splitted_date = input_date.split("/")
let month_needed = splitted_date[0]

let month_name = ["January","February","March","April","May","June","July","August","September","October","November","December"]

return month_name[month_needed-1]
}

module.exports = problem3
