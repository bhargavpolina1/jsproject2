let problem1 = function(inputString) {
    let inputStr = inputString
    let replacedDollar = inputStr.replace('$','')
    let replacedComma = replacedDollar.replace(',','')
    let convertedNum = parseFloat(replacedComma)

    let finalOp = ""
    if (convertedNum.toString().length === replacedComma.length){
        finalOp = convertedNum
    }

    else {
        finalOp = 0
}
return finalOp
}

module.exports = problem1
